#pragma once

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>
#include <cmath>

#include <cstdlib>
#include <functional>

#include <deque>
#include <list>
#include <vector>

using namespace std;


class Random	//Base Class # 2
{
public:
	int generateRandom(int max);
	int generateRandom0(int max);
	bool generateRandomBool();
};

