#ifndef __SokobanGame__entity__
#define __SokobanGame__entity__

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>
#include <cmath>

#include <cstdlib>
#include <functional>

#include <deque>
#include <list>
#include <vector>

using namespace std;

class entity		//Base Class #1
{
public:
	sf::RectangleShape rect;
	sf::Sprite sprite;
	sf::Text text;
	entity();
	~entity();
private:
protected:
};
#endif