#include "entity.h"

class brick : public entity							//derived class
{
public:
	double movementSpeed = 0.1;
	int direction = 0; // 1 - up, 2 - down, 3 - left, 4 - right
	bool canMoveUp = true;
	bool canMoveDown = true;
	bool canMoveLeft = true;
	bool canMoveRight = true;
	int lives = 0;

	brick();
	void update();
	void updateMovement();
};
