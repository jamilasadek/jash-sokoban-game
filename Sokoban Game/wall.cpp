#include "wall.h"

wall::wall(int tilesize)
{
	rect.setSize(sf::Vector2f(tilesize, tilesize));
	rect.setPosition(0, 0);
	rect.setFillColor(sf::Color::White);
	sprite.setPosition(rect.getPosition());
}


void wall::setposition(sf::Vector2f newpos){
	rect.setPosition(newpos);
	sprite.setPosition(newpos);
}
