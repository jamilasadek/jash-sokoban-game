#include "entity.h"

class player : public entity							//derived class
{
public:
	enum direct { up = 1, down = - 1, left = -2 , right = 2};
	double movementSpeed = 1;
	direct direction; // 1 - up, 2 - down, 3 - left, 4 - right
	bool canMoveUp = false;
	bool canMoveDown = false;
	bool canMoveLeft = false;
	bool canMoveRight = false;
	int lives = 0;

	player(int tilesize);
	void setposition(sf::Vector2f newpos);
	void updateMovement(direct dir);
	void restart();
};
