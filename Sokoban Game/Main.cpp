#include <iostream>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <fstream>
#include <string>
#include "player.h"
#include "entity.h"
#include "pickup.h"
#include "wall.h"
#include "Random.h"
#include "Menu.h"

using namespace std;
using namespace sf;

const string p1 = "map";
const string p2 = ".txt";
const int level_num = 5;
const int tilesize = 50;
int current_level = 0;
string winning = "";
bool lost = false;
bool take[5];


//Pickup Vector Array
vector<pickup> pickupArray;


// CREATING THE WALL(FRAME)
vector<wall> wallArray;
wall wall_o(tilesize);

//Create Destination
vector<wall> Destinations;
wall d(tilesize);

// CREATING THE BRICK
vector<player> BrickArray;
player BrickPlayer(tilesize);

//CREATE PLAYER 
player SuperMarioPlayer(tilesize);

void Initialize(Image&, RenderWindow& , Texture& , Texture& , Texture& , Texture& , Font& , Text& , Text& ,
	Text&, Text&, SoundBuffer&, Sound&, SoundBuffer&, Sound&, Music&, int&, bool&, vector<vector<string>>&, bool&, Text&, Text&, Text&, Text&, Text&, Text&, Text&, Text&, Text&);

void Restart(Text&, Text&, Text&, int&, bool&, Texture&, Texture&, Texture&, vector<string>&, Texture&, bool&, Text&, Text&, Text&, Text&, Text&, Text&, Text&, Text&, Text&);


bool playermove(sf::Sound &, sf::Sound &, bool&, int&, bool&, bool[]);
bool Brick_Dest(player&);
bool Check_boxes(vector<player>&, player&, int);
bool win();
void Openfile(string&, int, vector<vector<string>> &);
int main()
{
	//Variables
	srand(time(NULL));
	sf::Clock clock;
	
	sf::RenderWindow window(sf::VideoMode(1000,800), "Sokoban Game (super mario edition)");
	window.setPosition(sf::Vector2i(300, 20));
	
	sf::Image icon;
	
	sf::Texture SuperMario;
	sf::Texture Brick;
	sf::Texture life;
	sf::Texture textureWall;
	
	sf::Font font;
	
	sf::Text lives;
	sf::Text WIN;
	sf::Text steps;
	sf::Text levels;
	
	sf::Text Instructions;
	sf::Text LevelSelect;
	sf::Text restart;
	sf::Text exit;
	sf::Text one; 
	sf::Text two;
	sf::Text three;
	sf::Text four;
	sf::Text five;

	sf::SoundBuffer Bump;
	sf::Sound soundBump;
	sf::SoundBuffer Collectlife;
	sf::Sound soundCollectlife;
	
	sf::Music music;
	vector<vector<string>> map_array;
	int counterSteps = 0;
	bool playing;
	for (int i = 0; i < 5; i++)
		take[i] = false;
	take[1] = true;
	take[3] = true;
	bool won;

	Initialize(icon, window, SuperMario, Brick, life, textureWall, font, lives, WIN,
			  steps, levels, Bump, soundBump, Collectlife, soundCollectlife, music, counterSteps, playing, map_array, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

	// Start the game loop
	while (window.isOpen())
	{
		WIN.setString(winning);
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num1 && playing)
				{
					if (SuperMarioPlayer.lives <= 0)
						current_level = 0;

					else
					{
						SuperMarioPlayer.lives--;
						current_level = 0;
					}


					Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

				}

				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num2 && playing)
				{
					if (SuperMarioPlayer.lives <= 0)
						current_level = 0;

					else
					{
						SuperMarioPlayer.lives--;
						current_level = 1;
					}


					Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

				}

				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num3 && playing)
				{
					if (SuperMarioPlayer.lives <= 0)
						current_level = 0;

					else
					{
						SuperMarioPlayer.lives--;
						current_level = 2;
					}


					Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

				}

				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num4 && playing)
				{
					if (SuperMarioPlayer.lives <= 0)
						current_level = 0;

					else
					{
						SuperMarioPlayer.lives--;
						current_level = 3;
					}


					Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

				}

				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num5 && playing)
				{
					if (SuperMarioPlayer.lives <= 0)
						current_level = 0;

					else
					{
						SuperMarioPlayer.lives--;
						current_level = 4;
					}


					Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);

				}
			
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Up && playing)
			{
				counterSteps++;
				// Update Player
				SuperMarioPlayer.sprite.setTextureRect(sf::IntRect(380, 0, 28, 28));
				SuperMarioPlayer.updateMovement(player::up);
				playermove(soundBump, soundCollectlife, playing, counterSteps, won, take);
			}

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Right && playing)
			{
				counterSteps++;
				// Update Player
				SuperMarioPlayer.sprite.setTextureRect(sf::IntRect(110, 0, 28, 28));
				SuperMarioPlayer.updateMovement(player::right);
				playermove(soundBump, soundCollectlife, playing, counterSteps, won, take);
			}


			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Down && playing)
			{
				counterSteps++;
				// Update Player
				SuperMarioPlayer.sprite.setTextureRect(sf::IntRect(20, 0, 28, 28));
				SuperMarioPlayer.updateMovement(player::down);
				playermove(soundBump, soundCollectlife, playing, counterSteps, won, take);
			}

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space)
			{
				if (SuperMarioPlayer.lives <= 0)
					current_level = 0;

				else
				{
					SuperMarioPlayer.lives--;
				}


				Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);
				
			}

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Left && playing)
			{
				counterSteps++;
				// Update Player
				SuperMarioPlayer.sprite.setTextureRect(sf::IntRect(110, 0, 28, 28));
				SuperMarioPlayer.updateMovement(player::left);
				playermove(soundBump, soundCollectlife, playing, counterSteps, won, take);
			}


			// Escape pressed: exit
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) 
			{
				window.close();
			}
		}

		if (won)
		{
			if (current_level < 4)
			{
				Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[++current_level], life, won, Instructions, LevelSelect, restart, exit, one, two, three, four, five);
			}
		}

		// Clear screen
		window.clear();

		// Clock
		sf::Time elapsed1 = clock.getElapsedTime();

		// Draw Wall
		for (int j = 0; j < wallArray.size(); j++)
		{
			window.draw(wallArray[j].sprite);
		}

		for (int j = 0; j < Destinations.size(); j++)
		{
			window.draw(Destinations[j].rect);
		}

		//Draw Bricks
		for (int j = 0; j < BrickArray.size(); j++)
		{
			window.draw(BrickArray[j].sprite);
		}

		// Draw Player
		window.draw(SuperMarioPlayer.sprite);
		
		//Draw PickUp Items
		for (int i = 0; i < pickupArray.size(); i++)
		{
			if (!pickupArray[i].taken)
				window.draw(pickupArray[i].sprite);
		}
		

		// Life Text
		lives.setString("Lives: " + to_string(SuperMarioPlayer.lives));

		//Levels Text
		levels.setString(("Level ") + to_string(current_level+1));
		WIN.setString("WINNER!");
		steps.setString("Steps: " + counterSteps);
		Instructions.setString("MENU");
		LevelSelect.setString("Press the key associated with the following functions:");
		one.setString("1 for Level 1");
		two.setString("2 for Level 2");
		three.setString("3 for Level 3");
		four.setString("4 for Level 4");
		five.setString("5 for Level 5");
		restart.setString("To restart press Space Key");
		exit.setString("To Exit press the Esc Key");

/*
		//Clock
		if (elapsed1.asSeconds() >= "blank")
		{
			GameIsWon();
			clock.restart();
		}
*/	
		// Draw the string
		window.draw(lives);
		window.draw(levels);
		window.draw(Instructions);
		window.draw(LevelSelect);
		window.draw(one);
		window.draw(two);
		window.draw(three);
		window.draw(four);
		window.draw(five);
		window.draw(restart);
		window.draw(exit);


		//Draw the text WIN
		if (won)
		{
			if (current_level = 4)
			{
				window.draw(WIN);
			}
		}
		//cout << counterSteps;
		
		// Update the window
		window.display();
	}
	
	//system("pause");
	return EXIT_SUCCESS;
}

bool playermove(sf::Sound& soundBump, sf::Sound &soundCollectlife, bool& playing, int& counterSteps, bool& won, bool take[])
{
	// Player colliding with wall
	for (int j = 0; j < wallArray.size(); j++)
		if (SuperMarioPlayer.rect.getGlobalBounds().intersects(wallArray[j].rect.getGlobalBounds()))
		{
			// Hit Wall
			SuperMarioPlayer.updateMovement(player::direct(-int(SuperMarioPlayer.direction)));
			soundBump.play();
			counterSteps--;
			return false;
		}
	
	
	
	//Player Picks Up life
	for (int i = 0; i < pickupArray.size(); i++)
	{
		if (SuperMarioPlayer.rect.getGlobalBounds().intersects(pickupArray[i].rect.getGlobalBounds()))
		{
			if (!pickupArray[i].taken)
			{
				soundCollectlife.setVolume(30000);
				soundCollectlife.play();
				SuperMarioPlayer.lives += pickupArray[i].lifeValue;
				pickupArray[i].taken = true;
				take[current_level] = true;
			}
		}
	}
	//Player moves brick
	for (int n = 0; n < BrickArray.size(); n++)
	{
		if (SuperMarioPlayer.rect.getGlobalBounds().intersects(BrickArray[n].rect.getGlobalBounds()))
		{
			BrickArray[n].updateMovement(SuperMarioPlayer.direction);

			//Brick colliding with another brick

			if (Check_boxes(BrickArray, BrickArray[n], n))
			{
				BrickArray[n].updateMovement(player::direct(-int(BrickArray[n].direction)));
				SuperMarioPlayer.updateMovement(player::direct(-int(SuperMarioPlayer.direction)));
				soundBump.play();
				counterSteps--;
				return false;
			}

			//Brick colliding with a wall

			for (int j = 0; j < wallArray.size(); j++)
				if (BrickArray[n].rect.getGlobalBounds().intersects(wallArray[j].rect.getGlobalBounds()))
				{
					BrickArray[n].updateMovement(player::direct(-int(BrickArray[n].direction)));
					SuperMarioPlayer.updateMovement(player::direct(-int(SuperMarioPlayer.direction)));
					soundBump.play();
					counterSteps--;
					return false;
				}

			
			if (win())
			{
				playing = false;
				
				won = true;

			}
			return true;

		}
	}
	
}

bool win()
{
	for (int i = 0; i < BrickArray.size(); i++)
		if (!Brick_Dest(BrickArray[i]))
			return false;
	return true;
}

void Initialize(Image& icon, RenderWindow& window, Texture& SuperMario, Texture& Brick, Texture& life, Texture& textureWall, Font& font, Text& lives, Text& WIN,
				Text& steps, Text& levels, SoundBuffer& Bump,	Sound& soundBump, SoundBuffer& Collectlife, Sound& soundCollectlife, Music& music, int& counterSteps, 
				bool& playing, vector<vector<string>>& map_array, bool& won, Text& instructions, Text& LevelSelect, Text& restart, Text& exit, Text& one, Text& two, Text& three, Text& four, Text& five)
{
	map_array.resize(level_num);
	for (int i = 0; i < map_array.size(); i++)
		map_array[i].resize(0);
	for (int i = 0; i < level_num; i++)
	{
		string path = p1 + to_string(i + 1) + p2;
		Openfile(path, i, map_array);
	}

	SuperMarioPlayer.lives = 0;

	

	// Create the main window

	// Set the Icon
	if (icon.loadFromFile("icon.png")) 
	{
		window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	}

	// Load a Player to display
	SuperMario.loadFromFile("supermario.png");
		
	Brick.loadFromFile("block.png");

	life.loadFromFile("life.png");

	textureWall.loadFromFile("wall.png");


	font.loadFromFile("sansation.ttf");

	//Load Text
	lives.setCharacterSize(75);
	lives.setFont(font);

	WIN.setFont(font);
	WIN.setCharacterSize(100);	
	WIN.setPosition(490, 300);
	WIN.setColor(sf::Color::Cyan);

	levels.setFont(font);
	levels.setCharacterSize(125);
	levels.setPosition(490, 120);
	levels.setStyle(sf::Text::Bold);
	levels.setColor(sf::Color::White);

	instructions.setFont(font);
	instructions.setCharacterSize(50);
	instructions.setPosition(200, 450);
	instructions.setStyle(sf::Text::Bold);
	instructions.setStyle(sf::Text::Bold);
	instructions.setColor(sf::Color::Cyan);

	LevelSelect.setPosition(0, 510);
	LevelSelect.setFont(font);
	LevelSelect.setCharacterSize(20);
	LevelSelect.setColor(sf::Color::White);


	restart.setFont(font);
	restart.setCharacterSize(20);
	restart.setPosition(10, 650);
	restart.setColor(sf::Color::Yellow);

	exit.setFont(font);
	exit.setCharacterSize(20);
	exit.setPosition(10, 670);
	exit.setStyle(sf::Text::Bold);
	exit.setColor(sf::Color::Red);

	one.setFont(font);
	one.setCharacterSize(20);
	one.setPosition(10, 540);
	one.setColor(sf::Color::Red);

	two.setFont(font);
	two.setPosition(10, 560);
	two.setCharacterSize(20);
	two.setColor(sf::Color::Yellow);

	three.setFont(font);
	three.setCharacterSize(20);
	three.setPosition(10, 580);
	three.setColor(sf::Color::Cyan);

	four.setFont(font);
	four.setCharacterSize(20);
	four.setPosition(10, 600);
	four.setColor(sf::Color::Green);

	five.setFont(font);
	five.setCharacterSize(20);
	five.setPosition(10, 620);
	five.setColor(sf::Color::White);

	/*
	sf::Text clock("Time: ", font, 10);
	lives.setPosition(70, 70);
	lives.setColor(sf::Color::White);
	*/
	steps.setFont(font);
	steps.setCharacterSize(10);
	
	steps.setPosition(50, 50);
	steps.setColor(sf::Color::White);

	//load sounds to pl
	Bump.loadFromFile("bump_sound.wav");
	soundBump.setBuffer(Bump);

	Collectlife.loadFromFile("collectlife_sound.wav");
	soundCollectlife.setBuffer(Collectlife);

	// Load a music to play
	music.openFromFile("supermario_song.ogg");
	music.play();
	music.setVolume(50);
	music.setLoop(true);

	//Pickup Display Object

	Restart(lives, WIN, steps, counterSteps, playing, SuperMario, Brick, textureWall, map_array[current_level], life, won, instructions, LevelSelect, restart, exit, one, two, three, four, five);

}


void Restart(Text& lives, Text& WIN, Text& steps, int& counterSteps, bool& playing, Texture& SuperMario, Texture& Brick,
	Texture& textureWall, vector<string>& lines, Texture& life, bool& won, Text& instructions, Text& LevelSelect, Text& restart, Text& exit, Text& one, Text& two, Text& three, Text& four, Text& five)
{
	won = false;

	while(BrickArray.size() > 0 )
		BrickArray.pop_back();
	while (Destinations.size() > 0)
		Destinations.pop_back();
	while (wallArray.size() > 0)
		wallArray.pop_back();
	while (pickupArray.size() > 0)
		pickupArray.pop_back();
	SuperMarioPlayer.restart();

	for (int j = 0; j < BrickArray.size(); j++)
			BrickArray[j].restart();



	counterSteps = 0;
	playing = true;
	//Destinations.resize(0);

	// Load a Player to display

	lives.setString("lives: ");

	
	/*
	sf::Text clock("Time: ", font, 10);
	lives.setPosition(70, 70);
	lives.setColor(sf::Color::White);
	*/
	steps.setString("Steps: ");

	//load sounds to pl


	// Load a music to play


	winning = "";

	string line;
	
	for (int row = 0; row < lines.size(); row++)
	{
		line = lines[row];
		for (int col = 0; col < line.size(); col++)
		{
			char c = line[col];
			if (c == 'B')
			{
				BrickPlayer.sprite.setTexture(Brick);
				BrickPlayer.sprite.setTextureRect(sf::IntRect(0, 0, 50, 50));
				BrickPlayer.setposition(sf::Vector2f(col*tilesize, row*tilesize));
				BrickArray.push_back(BrickPlayer);
			}
			else if (c == '#')
			{
				wall_o.setposition(sf::Vector2f(col*tilesize, row*tilesize));
				wall_o.sprite.setTexture(textureWall);
				//				wall_o.rect.setScale(50 / textureWall.getSize().x, 50 / textureWall.getSize().y);
				wall_o.sprite.setScale((float)50 / textureWall.getSize().x, (float)50 / textureWall.getSize().y);
				wallArray.push_back(wall_o);
			}
			else if (c == 'L')
			{
				pickup L (col*tilesize, row*tilesize, take[current_level]);
				L.sprite.setTexture(life);
				pickupArray.push_back(L);
			}

			else if (c == 'D')
			{
				d.setposition(sf::Vector2f(col*tilesize, row*tilesize));
				
				Destinations.push_back(d);
			}

			else if (c == 'P')
			{
				SuperMarioPlayer.sprite.setTexture(SuperMario);
				SuperMarioPlayer.setposition(sf::Vector2f(col*tilesize, row*tilesize));
			}
			else if (c == 'H')
			{

				lives.setPosition(sf::Vector2f(col*tilesize, row*tilesize));
				lives.setColor(sf::Color::Red);
			}
			
		}
	}
}


void Openfile(string& path, int i, vector<vector<string>>& map_array)
{
	ifstream openfile;
	openfile.open(path.c_str());
	
	if (!openfile.fail())
	{
		string line;
		do
		{
			getline(openfile, line);
			map_array[i].push_back(line);	
		} while (!openfile.eof());
		openfile.close();
	}

}

bool Check_boxes(vector<player>& BrickArray, player& b, int n)
{
	for (int i = 0; i < BrickArray.size(); i++)
		if (b.rect.getGlobalBounds().intersects(BrickArray[i].rect.getGlobalBounds()) && n != i)
			return true;
	return false;

}

bool Brick_Dest(player& b)
{
	for (int i = 0; i < Destinations.size(); i++)
	{
		if (b.rect.getGlobalBounds().intersects(Destinations[i].rect.getGlobalBounds()))
			return true;
	}
	return false;

}