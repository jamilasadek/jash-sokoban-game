#include "player.h"

player::player(int tilesize)
{
	movementSpeed = tilesize;
	rect.setSize(sf::Vector2f(28, 28));
	restart();
	
}

void player::setposition(sf::Vector2f newpos){
	rect.setPosition(newpos);
	sprite.setPosition(newpos);
}

void player::restart()
{
	rect.setPosition(4000, 4000);
	sprite.setPosition(rect.getPosition());
	sprite.setTextureRect(sf::IntRect(20, 0, 28, 28));

}

void player::updateMovement(direct dir)
{
	direction = dir;
	switch (dir){
	case up:
	{
			   rect.move(0, -movementSpeed);
			   sprite.setPosition(rect.getPosition());
			  // sprite.setTextureRect(sf::IntRect(380, 0, 28, 28));

	}
		break;

	case down:
	{
				 rect.move(0, movementSpeed);
				 sprite.setPosition(rect.getPosition());
				// sprite.setTextureRect(sf::IntRect(20, 0, 28, 28));
	}
		break;

	case left:
	{
				 rect.move(-movementSpeed, 0);
				 sprite.setPosition(rect.getPosition());
				 //sprite.setTextureRect(sf::IntRect(110, 0, 28, 28));
	}
		break;

	case right:
	{
				  rect.move(movementSpeed, 0);
				  sprite.setPosition(rect.getPosition());
				  //sprite.setTextureRect(sf::IntRect(110, 0, 28, 28));
	}
		break;
	}
}
